package com.example;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.reactivex.Maybe;

@Controller
public class HelloWorld {
    @Get("/echo/{s}")
    public String echo(String s) {
        return s;
    }
}
